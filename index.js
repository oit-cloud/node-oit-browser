var bonjour = require("bonjour");
var debug = require("debug")("oit-browser");
var net = require("net");
var xtend = require("xtend");
var EventEmitter = require("events").EventEmitter;

var sockets = {};

var seq = 0;

module.exports = function() {
  var ee = new EventEmitter();

  bonjour = bonjour();

  var browser = bonjour.find({
    type: "oitdevice"
  });

  browser.on("up", function(service) {
    // address selection improvements
    // 1) should try another address on failure
    // 2) would be nice to figure out ideal address
    var address = service.addresses[0];
    var name = service.fqdn;
    var port = service.port;

    var socket;
    
    if((socket = sockets[name])) {
      queryDevice();
    } else {
      socket = sockets[name] = net.createConnection(port, address);

      socket.once("connect", function() {
        debug("Socket connected for %s", name);

        queryDevice();
      });

      var wrapper;

      socket.on("data", function(data) {
        switch(data[1]) {
          case 0:
            for(var i = 2, idx = 0; i < 32; i += 3, idx++) {
              var devDesc = data.slice(i, 3 + i);
              if(devDesc[1] === 0) return;

              wrapper = serviceWrapper({
                index: idx,
                name: name,
                type: devDesc[1],
                value: devDesc[2],
                txtRecord: service.txt
              });

              ee.emit("serviceUp", wrapper);
            }

          break;

          case 1:
            if(wrapper) {
              wrapper.value = data[4];
            }
        }
      });

      socket.on("end", function(e) {
        ee.emit("down", service);
      });
    }

    function queryDevice() {
      if(!socket) {
        return console.error("Device queried before existing.");
      }

      socket.write(new Buffer([ seq++ % 255, 0x00 ]));
    }
  });

  browser.on("down", function(service) {
    var name = service.fqdn;

    if(sockets[name]) {
      debug("Cleaning up socket for %s", name);

      sockets[name].end();
      delete sockets[name];

      ee.emit("serviceDown", {
        name: service.name,
        txtRecord: service.txt
      });
    }
  });

  browser.on("error", function(err) {
    ee.emit("error", err);
  });

  browser.start();

  ee.end = function() {
    bonjour.unpublishAll();
    bonjour.destroy();

    for(var name in sockets) {
      sockets[name].end();
      delete sockets[name];
    }

    browser.stop();
  };

  return ee;
};

function serviceWrapper(desc) {
  var service = xtend(desc, {
    set: function(v, cb) {
      var socket = sockets[service.name];

      if(socket) {
        var buf = new Buffer(7);

        buf[0] = seq++ % 255;
        buf[1] = 0x01;
        buf[2] = service.index || 0

        buf.writeUInt32LE(v, 3);

        socket.write(buf);
      }
    }
  });

  return service;
}
