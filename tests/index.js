var EventEmitter = require("events").EventEmitter;

var oitCloud = require("../")();
var oitService = require("oit-service");
var test = require("tape");

oitCloud.on("error", console.log.bind(console));

var testId = "" + Math.round(Math.random() * 10000);

var type = 1;
var value = 0;

var service;

test(function(t) {
  t.plan(6);

  oitCloud.on("serviceUp", onServiceUp);

  var testEE = new EventEmitter();

  function onServiceUp(service) {
    if(service.txtRecord.oittest) {
      oitCloud.removeListener("serviceUp", onServiceUp);

      t.equal(service.txtRecord.oittest, testId, "matching testId in serviceUp");
      t.equal(service.name, "oitdevice._oitdevice._tcp.local", "name matched");
      t.equal(service.type, type, "type matched");
      t.equal(service.value, value, "value matched");

      testEE.on("change", function(v) {
        t.equal(v, 1, "test value emitted from handler");
        t.equal(service.value, 1, "value updated on service");
      });

      service.set(1);
    }
  }

  var oitservice = service = oitService([{
    type: 1,

    getValue: function() {
      return value;
    },

    setValue: function(v, cb) {
      value = v ? 1 : 0;

      this.value = value;
      cb();

      setTimeout(function() {
        testEE.emit("change", value);
      }, 13);
    }
  }], {
    adOpts: {
      txtRecord: {
        oitTest: testId
      }
    }
  });
}, "serviceUp");

test(function(t) {
  t.plan(3);

  oitCloud.on("serviceDown", onServiceDown);
  service.stop(function() {
    t.pass("sockets closed"); 

    oitCloud.end();
  });

  function onServiceDown(service) {
    if(service.txtRecord.oittest) {
      t.equal(service.txtRecord.oittest, testId, "matching testId");
      oitCloud.removeListener("serviceDown", onServiceDown);

      t.pass("cleanup complete");
    }
  }
}, "serviceDown / cleanup");
