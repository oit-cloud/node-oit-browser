var debug = require("debug")("oit-browser-cli");
var fs = require("fs");
var oitBrowser = require("../");

var TYPE = [
  undefined,
  "Boolean",
  "PWM"
];

var argv = require("minimist")(process.argv, {
  alias: {
    l: [ "ls", "list" ],
    q: [ "query" ],
    t: [ "timeout" ],
    v: [ "val", "value" ]
  }
});

var doTimeout = false;

if(argv.l) {
  console.log("Listing...");
  console.log("CTRL-C to end.");

  var browser = oitBrowser();
  browser.on("serviceUp", function(service) {
    console.log("up: (%s)\t%s.%d %s", TYPE[service.type], service.name, service.index, service.value);
  });

  var interrupted = false;

  process.on("SIGINT", function() {
    if(!interrupted) {
      console.log("\nexiting");
      interrupted = !interrupted;

      browser.end(function() {
        console.log("done");
      });
    } else {
      console.log("\nbailing");
      process.exit(1);
    }
  });

  doTimeout = true;
} else if(argv.q) {
  if(!argv.v && argv.v !== 0) {
    console.log("Query requires a value (-v, --value).");
    process.exit(2);
  }

  var browser = oitBrowser();
  browser.on("serviceUp", function(service) {
    var identifier = service.name + "." + service.index;

    if(argv.q === identifier) {
      debug("%s found", identifier);
      service.set(argv.v);
      console.log("%s set to %s", identifier, argv.v);

      browser.end();
    }
  });

  doTimeout = true;
} else {
  fs.createReadStream("./bin/usage.txt").pipe(process.stdout);
}

if(doTimeout && argv.t) {
  var timeout = (parseInt(argv.t) * 1000) || 6000;

  setTimeout(function() {
    process.emit("SIGINT");
  }, timeout);
}
